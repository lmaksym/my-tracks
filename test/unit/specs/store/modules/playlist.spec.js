import playlistStore from '../../../../../src/store/modules/playlist';
import * as types from '../../../../../src/store/types';
import {Playlist} from '../../../../../src/classes/playlist';
import playlist from '../../../../../src/api/playlist';
import {testAction} from '../../../helpers/testAction';
import {LIST_ITEM_STATES} from '../../../../../src/constants/index';

const receivePlaylist = playlistStore.mutations[types.RECEIVE_PLAYLIST];
const addNewToPlaylist = playlistStore.mutations[types.ADD_NEW_TO_PLAYLIST];
const editItemInPlaylist = playlistStore.mutations[types.EDIT_ITEM_PLAYLIST];

describe('playlist module', () => {
  let state;
  beforeEach(() => {
    state = {
      playlist: [],
      isFetching: false,
      error: null
    };
  });
  describe('playlist getters', () => {
    it('should return existing playlist', () => {
      let playlistItem = new Playlist(`Test`, 2);
      state.playlist = [playlistItem];
      expect(playlistStore.getters.allPlaylist(state)).toHaveLength(1);
      expect(playlistStore.getters.allPlaylist(state)).toContainEqual(playlistItem);
    });
  });
  describe('playlist mutations', () => {
    it('should add to state plailis 10 items', () => {
      const playlist = [];
      let i = 0;
      while (i < 10) {
        playlist.push(new Playlist(`Test${i}`, i));
        i++;
      }
      receivePlaylist(state, {playlist});
      expect(state.playlist).toHaveLength(10);
    });
    it('should set state is fetching false and error null', () => {
      receivePlaylist(state, {playlist: []});
      expect(state.error).toBeNull();
      expect(state.isFetching).toBeFalsy();
    });
    it('should contain new playlist object with status NEW', () => {
      addNewToPlaylist(state);
      let newItem = new Playlist(null, null, null, LIST_ITEM_STATES.NEW);
      expect(state.playlist).toContainEqual(newItem);
    });
    it('should contain element with status EDIT', () => {
      const playlist = [];
      let i = 0;
      while (i < 10) {
        playlist.push(new Playlist(`Test${i}`, i));
        i++;
      }
      state.playlist = playlist.map(elem => elem);
      editItemInPlaylist(state, {id: 3});
      let playlistItem = state.playlist.find(elem => elem.id === 3);
      expect(playlistItem.state).toEqual(LIST_ITEM_STATES.EDIT);
    });
  });

  describe('playlist actions', () => {
    it('should load playlist and call receive mutation', (done) => {
      playlist.getPlaylist = jest.fn((page) => new Promise(resolve => {
        resolve({data: {items: [{id: 1, name: 'test', images: []}]}});
      }));
      let playlistItems = [new Playlist('test', 1)];
      testAction(playlistStore.actions.getAllPlaylist, null, {state}, [
        {type: types.RECEIVE_PLAYLIST, payload: {playlist: playlistItems}}
      ], [], done);
    });
    it('should call ADD_NEW_TO_PLAYLIST mutation', (done) => {
      testAction(playlistStore.actions.addNewItemToPlaylist, null, {state}, [
        {type: types.ADD_NEW_TO_PLAYLIST}
      ], [], done);
    });
    it('should call EDIT_ITEM_PLAYLIST mutation', (done) => {
      testAction(playlistStore.actions.editItemInList, 3, {state}, [
        {type: types.EDIT_ITEM_PLAYLIST, payload: {id: 3}}
      ], [], done);
    });
    it('should dispatch getAllPlaylist action after save', (done) => {
      const getters = {getters: {userId: 1}};
      playlist.save = jest.fn((userId, name) => new Promise(resolve => {
        resolve('done');
      }));
      testAction(playlistStore.actions.saveItemInPlayList, {name: 'Test'}, {state, getters}, [], [
        {action: 'getAllPlaylist'}
      ], done);
    });
    it('should dispatch getAllPlaylist action after update', (done) => {
      const getters = {getters: {userId: 1}};
      playlist.update = jest.fn((userId, item) => new Promise(resolve => {
        resolve('done');
      }));
      testAction(playlistStore.actions.updateItemInPlayList, {item: {}}, {state, getters}, [], [
        {action: 'getAllPlaylist'}
      ], done);
    });
  });
});
