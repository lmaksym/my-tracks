// helper for testing action with expected mutations
export const testAction = (action,
                           payload,
                           {state, rootState, getters},
                           expectedMutations = [],
                           expectedDispatchers = [],
                           done) => {
  let count = 0;
  let dispCount = 0;

  // mock commit
  const commit = (type, payload) => {
    const mutation = expectedMutations[count];
    try {
      expect(mutation.type).toEqual(type);
      if (payload) {
        expect(mutation.payload).toEqual(payload);
      }
    } catch (error) {
      done(error);
    }

    count++;
    if (count >= expectedMutations.length) {
      done();
    }
  };

  // mock dispatch
  const dispatch = (actionType, payload) => {
    const extAction = expectedDispatchers[dispCount];
    try {
      expect(extAction.action).toEqual(actionType);
      if (payload) {
        expect(extAction.payload).toEqual(payload);
      }
    } catch (error) {
      done(error);
    }

    dispCount++;
    if (dispCount >= expectedDispatchers.length) {
      done();
    }
  };

  const context = {
    state,
    rootState,
    commit,
    dispatch,
    getters
  };

  // call the action with mocked store and arguments
  action(context, payload);

  // check if no mutations and dispatchers should have been dispatched
  if (expectedMutations.length === 0 && expectedDispatchers.length === 0) {
    expect(count).toEqual(0);
    done();
  }
};
