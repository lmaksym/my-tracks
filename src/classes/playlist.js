import {LIST_ITEM_STATES} from '../constants/index';

export class Playlist {
  constructor (name,
              id,
              image = '',
              state = LIST_ITEM_STATES.EXIST) {
    this.name = name;
    this.id = id;
    this.image = image || '/src/assets/default_playlist_image.png';
    this.state = state;
  }
}
