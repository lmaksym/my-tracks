export class Track {
  constructor (id, name, album, uri) {
    this.id = id;
    this.name = name;
    this.album = album;
    this.uri = uri;
  }
}
