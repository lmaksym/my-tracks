import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import Vuetify from 'vuetify';
import api from './api/api.init';
import Vuelidate from 'vuelidate';

Vue.config.productionTip = false;

Vue.use(Vuetify);
Vue.use(Vuelidate);
api.init();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
