import Vue from 'vue';
import Vuex from 'vuex';
import playlist from './modules/playlist';
import auth from './modules/auth';
import tracks from './modules/trackList';
import externalTracks from './modules/externalTrackList';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    playlist,
    auth,
    tracks,
    externalTracks
  }
});
export default store;
