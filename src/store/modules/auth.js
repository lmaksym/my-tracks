import * as types from '../types';
import auth from '../../api/auth';
import router from '../../router';

const state = {
  isLoggedIn: auth.isLoggedIn(),
  isFetching: false,
  userId: auth.getUserId(),
  error: null
};
// getters
const getters = {
  isLoggedIn: state => state.isLoggedIn,
  userId: state => state.userId
};

// actions
const actions = {
  loginUser ({dispatch, commit}) {
    auth.login()
      .then(() => {
        commit(types.LOGIN_USER, {isLoggedIn: true});
        dispatch('setUserInfo');
        router.push('/');
      });
  },
  setUserInfo ({commit}) {
    auth.setUserInfo()
      .then((response) => {
        const userId = response.data.id;
        localStorage.setItem('userId', userId);
        commit(types.SET_USER_ID, {userId});
      });
  },
  logout ({commit}) {
    auth.logout()
      .then(() => {
        router.push('/welcome');
        commit(types.LOGOUT_USER);
      });
  }
};

// mutations
const mutations = {
  [types.SET_USER_ID] (state, { userId }) {
    state.userId = userId;
    state.isFetching = false;
    state.error = null;
  },
  [types.LOGIN_USER] (state, { isLoggedIn }) {
    state.isLoggedIn = isLoggedIn;
    state.isFetching = false;
    state.error = null;
  },
  [types.LOGOUT_USER] (state) {
    state.isLoggedIn = false;
    state.isFetching = false;
    state.userId = null;
    state.error = null;
  }
};
export default {
  state,
  getters,
  actions,
  mutations
};
