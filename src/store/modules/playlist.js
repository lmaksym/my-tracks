import playlist from '../../api/playlist';
import * as types from '../types';
import {Playlist} from '../../classes/playlist';
import {LIST_ITEM_STATES} from '../../constants/index';

const state = {
  playlist: [],
  isFetching: false,
  error: null
};
// getters
const getters = {
  allPlaylist (state) {
    return state.playlist;
  }
};

// actions
const actions = {
  getAllPlaylist ({commit}) {
    return playlist.getPlaylist()
      .then((response) => {
        commit(types.RECEIVE_PLAYLIST, {
          playlist: response.data.items.map((elem) => {
            return new Playlist(elem.name, elem.id,
              (elem.images.length && elem.images[0].url));
          })
        });
      });
  },
  addNewItemToPlaylist ({commit}) {
    commit(types.ADD_NEW_TO_PLAYLIST);
  },
  editItemInList ({commit}, id) {
    commit(types.EDIT_ITEM_PLAYLIST, {id});
  },
  saveItemInPlayList ({dispatch, getters}, {name}) {
    return playlist.save(getters.userId, name)
      .then(() => {
        dispatch('getAllPlaylist');
      });
  },
  updateItemInPlayList ({dispatch, getters}, item) {
    playlist.update(getters.userId, item)
      .then(() => {
        dispatch('getAllPlaylist');
      });
  }
};

// mutations
const mutations = {
  [types.ADD_NEW_TO_PLAYLIST] (state) {
    let playlist = state.playlist.map(element => element);
    playlist.unshift(new Playlist(null, null, null, LIST_ITEM_STATES.NEW));
    state.playlist = playlist;
    state.isFetching = false;
    state.error = null;
  },
  [types.EDIT_ITEM_PLAYLIST] (state, {id}) {
    state.playlist = state.playlist.map((element) => {
      if (element.id === id) {
        element.state = LIST_ITEM_STATES.EDIT;
      }
      return element;
    });
    state.isFetching = false;
    state.error = null;
  },
  [types.RECEIVE_PLAYLIST] (state, {playlist}) {
    state.playlist = playlist;
    state.isFetching = false;
    state.error = null;
  }
};
export default {
  state,
  getters,
  actions,
  mutations
};
