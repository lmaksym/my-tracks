import tracks from '../../api/tracks';
import { Track } from '../../classes/track';
import * as types from '../types';

const state = {
  externalTrackList: [],
  isFetching: false,
  error: null
};
// getters
const getters = {
  allExternalTrackList: state => state.externalTrackList
};

// actions
const actions = {
  getAllExternalTrackList ({commit}) {
    tracks.getTrackList()
      .then((response) => {
        let items = response.data.tracks.items.map((element) => {
          return new Track(element.id,
            element.name,
            element.album,
            element.uri);
        });
        commit(types.RECEIVE_EXTERNAL_TRACKLIST, {
          trackList: items
        });
      });
  },
  addTrackToList ({commit, getters}, {trackUri, playlistId}) {
    tracks.addTrackToList(trackUri, playlistId, getters.userId)
      .then((response) => {
        commit(types.EXTERNAL_TRACKLIST_TO_LIST, {response});
      });
  }
};

// mutations
const mutations = {
  [types.EXTERNAL_TRACKLIST_TO_LIST] (state, {response}) {
    state.isFetching = false;
    state.error = null;
  },
  [types.RECEIVE_EXTERNAL_TRACKLIST] (state, {trackList}) {
    state.externalTrackList = trackList;
    state.isFetching = false;
    state.error = null;
  }
};
export default {
  state,
  getters,
  actions,
  mutations
};
