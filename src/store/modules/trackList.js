import tracks from '../../api/tracks';
import { Track } from '../../classes/track';
import * as types from '../types';

const state = {
  trackList: [],
  playListItem: null,
  isFetching: false,
  error: null
};
// getters
const getters = {
  allTrackList: state => state.trackList,
  playListItem: state => state.playListItem
};

// actions
const actions = {
  getAllTrackList ({commit, getters}, id) {
    commit(types.CLEAR_TRACKLIST);
    tracks.getTrackListByPlayListId(id, getters.userId)
      .then((response) => {
        let items = response.data.tracks.items.map((element) => {
          let {id, name, album, uri} = element.track;
          return new Track(id, name, album, uri);
        });
        commit(types.RECEIVE_TRACKLIST, {
          trackList: items
        });
        commit(types.RECEIVE_PLAYLIST_ITEM, {
          playListItem: {
            name: response.data.name,
            id: response.data.id
          }
        });
      });
  },
  deleteTrackFromList ({commit, dispatch, getters}, {trackUri, playlistId}) {
    tracks.deleteTrack(trackUri, playlistId, getters.userId)
      .then(() => {
        commit(types.DELETE_TRACK_FROM_LIST);
        dispatch('getAllTrackList', playlistId);
      });
  }
};

// mutations
const mutations = {
  [types.RECEIVE_TRACKLIST] (state, {trackList}) {
    state.trackList = trackList;
    state.isFetching = false;
    state.error = null;
  },
  [types.DELETE_TRACK_FROM_LIST] (state) {
    state.isFetching = false;
    state.error = null;
  },
  [types.RECEIVE_PLAYLIST_ITEM] (state, {playListItem}) {
    state.playListItem = playListItem;
    state.isFetching = false;
    state.error = null;
  },
  [types.CLEAR_TRACKLIST] (state) {
    state.trackList = [];
    state.playListItem = null;
    state.isFetching = false;
    state.error = null;
  }
};
export default {
  state,
  getters,
  actions,
  mutations
};
