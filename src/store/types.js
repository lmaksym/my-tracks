export const CHECKOUT_REQUEST = 'CHECKOUT_REQUEST';
export const CHECKOUT_SUCCESS = 'CHECKOUT_SUCCESS';
export const CHECKOUT_FAILURE = 'CHECKOUT_FAILURE';

export const RECEIVE_PLAYLIST = 'RECEIVE_PLAYLIST';
export const RECEIVE_PLAYLIST_ITEM = 'RECEIVE_PLAYLIST_ITEM';
export const ADD_NEW_TO_PLAYLIST = 'ADD_NEW_TO_PLAYLIST';
export const EDIT_ITEM_PLAYLIST = 'EDIT_ITEM_PLAYLIST';

export const RECEIVE_TRACKLIST = 'RECEIVE_TRACKLIST';
export const DELETE_TRACK_FROM_LIST = 'DELETE_TRACK_FROM_LIST';
export const CLEAR_TRACKLIST = 'CLEARE_TRACKLIST';

export const LOGIN_USER = 'LOGIN_USER';
export const SET_USER_ID = 'SET_USER_ID';
export const LOGOUT_USER = 'LOGOUT_USER';

export const RECEIVE_EXTERNAL_TRACKLIST = 'RECEIVE_EXTERNAL_TRACKLIST';
export const EXTERNAL_TRACKLIST_TO_LIST = 'EXTERNAL_TRACKLIST_TO_LIST';


