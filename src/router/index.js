import Vue from 'vue';
import Router from 'vue-router';
import Playlist from '@/components/Playlist';
import TrackList from '@/components/TrackList';
import Welcome from '@/components/Welcome';
import LoginCallback from '@/components/LoginCallback';
import TrackListAdd from '@/components/TrackListAdd';
import { authGuard } from './authGuard';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: __dirname,
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: {name: 'playlist'}
    },
    {
      exact: true,
      path: '/playlist',
      name: 'playlist',
      component: Playlist,
      beforeEnter: authGuard
    },
    {
      exact: true,
      path: '/playlist/:id',
      name: 'tracks',
      component: TrackList,
      beforeEnter: authGuard
    },
    {
      exact: true,
      path: '/playlist/:id/add',
      name: 'addTracks',
      component: TrackListAdd,
      beforeEnter: authGuard
    },
    {
      path: '/welcome',
      name: 'welcome',
      component: Welcome,
      beforeEnter: authGuard
    },
    {
      path: '/login_callback',
      name: 'loginCallback',
      component: LoginCallback
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});
export default router;
