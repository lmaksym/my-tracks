import store from '../store';

export const authGuard = (to, from, next) => {
  let isLoggedIn = store.getters.isLoggedIn;
  if (!isLoggedIn && to.name !== 'welcome') {
    next({name: 'welcome'});
  } else if (isLoggedIn && to.name === 'welcome') {
    next({name: 'playlist'});
  } else {
    next();
  }
};
