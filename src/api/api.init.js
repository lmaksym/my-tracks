import axios from 'axios';
import {BASE_URL} from './api.config';
import store from '../store';

const api = {
  setBaseUrl () {
    axios.defaults.baseURL = BASE_URL;
  },
  setInterseptors () {
    axios.interceptors.response.use(function (response) {
      return response;
    }, function (error) {
      if (error.response.status === 401) {
        store.dispatch('logout');
      }
      if (error.response.status === 404) {
        alert(error.response.message);
      }
      return Promise.reject(error);
    });
  },
  init () {
    this.setInterseptors();
    this.setBaseUrl();
  }
};

export default api;
