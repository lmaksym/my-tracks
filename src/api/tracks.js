import axios from 'axios';

export default {
  getTrackListByPlayListId (id, userId) {
    const URL = `users/${userId}/playlists/${id}`;
    return axios.get(URL);
  },
  getTrackList () {
    const URL = 'search?q=new&type=track';
    return axios.get(URL);
  },
  addTrackToList (uris, playlistId, userId) {
    const URL = `users/${userId}/playlists/${playlistId}/tracks`;
    return axios.post(URL, {uris: [uris]});
  },
  deleteTrack (uris, playlistId, userId) {
    const URL = `users/${userId}/playlists/${playlistId}/tracks`;
    return axios.delete(URL, {data: {uris: [uris]}});
  }
};
