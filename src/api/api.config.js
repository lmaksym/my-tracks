export const AUTH_URL = 'https://accounts.spotify.com/en/authorize';
export const BASE_URL = 'https://api.spotify.com/v1/';
export const USER_URL = `me`;
export const PLAYLIST_URL = `me/playlists`;
