import axios from 'axios';
import {PLAYLIST_URL} from './api.config';

export default {
  getPlaylist () {
    return axios.get(PLAYLIST_URL);
  },
  save (userId, name) {
    const url = `users/${userId}/playlists`;
    return axios.post(url, {name, public: true});
  },
  update (userId, item) {
    const url = `users/${userId}/playlists/${item.id}`;
    return axios.put(url, {name: item.name, public: true});
  }
};
