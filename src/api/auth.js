import axios from 'axios';
import {AUTH_URL, USER_URL} from './api.config';

const authData = {
  clientId: '89025eaa5e9e41c9a6160cc140945d12',
  redirect_uri: 'http://localhost:8081/login_callback',
  response_type: 'token',
  base_url: AUTH_URL
};

export default {
  login () {
    let urlLogin = authData.base_url + '?response_type=token&client_id=' +
      authData.clientId + '&redirect_uri=' + encodeURIComponent(authData.redirect_uri) +
      '&scope=user-read-private user-read-email playlist-modify-public playlist-modify-private';
    return new Promise((resolve, reject) => {
      let authWindow = openDialog(urlLogin, 'Spotify', '', () => {
        if (!this.isLoggedIn()) {
          return reject(Error('Unexpected error'));
        }
      });
      window.addEventListener('storage', storageChanged);
      function storageChanged (e) {
        if (e.key === 'spotify-token') {
          if (authWindow) { authWindow.close(); }
          window.removeEventListener('storage', storageChanged, false);
          return resolve(this.isLoggedIn);
        }
      }
    });
  },
  logout () {
    return new Promise((resolve) => {
      localStorage.removeItem('spotify-token');
      localStorage.removeItem('userId');
      delete axios.defaults.headers.common[ 'Authorization' ];
      resolve(true);
    });
  },
  isLoggedIn () {
    let commonHeaders = axios.defaults.headers.common;
    let token = localStorage.getItem('spotify-token');
    if (token && !commonHeaders[ 'Authorization' ]) {
      commonHeaders[ 'Authorization' ] = 'Bearer ' + token;
    }
    return !!token;
  },
  setUserInfo () {
    return axios.get(USER_URL);
  },
  getUserId () {
    return localStorage.getItem('userId');
  }
};

function openDialog (uri, name, options, cb) {
  let win = window.open(uri, name, 'menubar=no,location=no,resizable=yes,scrollbars=yes,status=no');
  let interval = window.setInterval(function () {
    try {
      if (!win || win.closed) {
        window.clearInterval(interval);
        cb(win);
      }
      if (win && win.location.hash && win.location.hash.startsWith('#access_token')) {
        let token = win.location.hash.replace('#access_token=', '');
        window.clearInterval(interval);
        win.localStorage.setItem('spotify-token', token);
        cb(win);
      }
    } catch (e) {}
  }, 1000);
  return win;
}
